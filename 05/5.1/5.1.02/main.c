#include <stdio.h>

int main(void)
{
    int x, s1, s2, v;

    scanf("%d", &x);

    s1 = x * x;
    s2 = 6 * s1;
    v = x * x * x;

    printf("%d %d %d", s1, s2, v);

    return 0;
}
