Статистика куба
==============

Дана длина ребра куба. Необходимо найти площадь одной грани, площадь полной поверхности и объем этого куба.
​

---
## Входные данные ##

*x* - длина ребра куба, целое число.


---
## Выходные данные ##

Три числа, записанные через пробел. Первое - площадь грани куба, второе - площадь полной поверхности куба, третье число - объем куба.

---
## Sample Input: ##
```
2
```

---
## Sample Output: ##
```
4 24 8
```
