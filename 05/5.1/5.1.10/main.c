#include <stdio.h>

int main(void)
{
    int K;
    long bin_gb, dec_gb, difference;
    long bin_1gb = 1073741824L;
    long dec_1gb = 1000000000L;

    scanf("%d", &K);

    bin_gb = K * bin_1gb;
    dec_gb = K * dec_1gb;
    difference = bin_gb - dec_gb;

    printf("%ld", difference);

    return 0;
}
