#include <stdio.h>

int main(void)
{
    int x, r, t;

    scanf("%d", &x);

    r = 2 * x;
    t = 2 * r / 900;
    printf("%d %d", r, t);

    return 0;
}
