Ширина и площадь кольца
=====================

Как известно планета Сатурн имеет систему концентрических колец, состоящих из льда и пыли. Впервые их обнаружил ещё Галилео Галилей. Кольца разделены между собой щелями, иногда довольно большими. Напишите программу, которая по известным значениям внутреннего и внешнего радиусов кольца вычисляет его ширину и площадь.


---
## Входные данные ##
Два натуральных числа. Первое $r_i$ - внутренний радиус кольца в километрах, второе $r_o$ - внешний радиус кольца в километрах.


---
## Выходные данные ##
Два числа. Первое число - ширина кольца в километрах (целое число). Второе число площадь кольца в **гектарах**. Формат: два знака после запятой.


---
## Подсказки ##
Площадь круга вычисляется по формуле $S_{к} = \pi \cdot R^2$, где $R$ - радиус окружности.  
1 кв. км = 100 Га  
Для числа $\piπ$ используйте значение 3.14159265358979323846  
Если будут получаться отрицательные или слишком большие числа, то значение квадрата радиуса превосходит размеры типа `int`. Используйте явное преобразование к типу `long float`/`double` в произведении, так как мы делали при делении.
​

---
## Sample Input: ##
```
122200 136800
```

---
## Sample Output: ##
```
14600 1187961846028.44
```
