#include <stdio.h>

int main(void)
{
    int ri, ro, width;
    double s;
    double pi = 3.14159265358979323846;

    scanf("%d%d", &ri, &ro);

    width = ro - ri;
    s = pi * ((double)ro * ro - (double)ri * ri) * 100;

    printf("%d %.2lf", width, s);

    return 0;
}
