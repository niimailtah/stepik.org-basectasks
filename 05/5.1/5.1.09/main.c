#include <stdio.h>

int main(void)
{
    int A, a1, a2, a3, a4;
    double x;

    scanf("%d", &A);

    a1 = A % 10;
    a2 = (A / 10) % 10;
    a3 = (A / 100) % 10;
    a4 = A / 1000;
    x = ((double)a4 * a2) / ((double)a1 * a3);

    printf("%.2lf", x);

    return 0;
}
