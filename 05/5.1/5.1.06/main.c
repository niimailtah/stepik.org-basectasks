#include <stdio.h>

int main(void)
{
    int N, drops_num;
    double molecules_num;
    double drop = 0.05;
    double glass = 249.5;
    double molecule = 3e-23;

    scanf("%d", &N);

    drops_num = N * glass / drop;
    molecules_num = N * glass / molecule;

    printf("%d %.3e", drops_num, molecules_num);

    return 0;
}
