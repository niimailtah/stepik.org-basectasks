#include <stdio.h>

int main(void)
{
    double base, fourth, fifth, sixth;

    base = (double)1 + ((double)1 / 1) + ((double)1 / (1 * 2));
    fourth = (double)1 / ((double)1 * 2 * 3);
    fifth  = (double)1 / ((double)1 * 2 * 3 * 4);
    sixth  = (double)1 / ((double)1 * 2 * 3 * 4 * 5);

    printf("%.5lf\n%.5lf\n%.5lf", base + fourth, base + fourth + fifth, base + fourth + fifth + sixth);

    return 0;
}
