#include <stdio.h>

int main(void)
{
    int K, first_digit, second_digit, third_digit, res;

    scanf("%d", &K);

    first_digit = K % 10;
    second_digit = (K / 10) % 10;
    third_digit = K / 100;
    res = first_digit * 100 + second_digit * 10 + third_digit;

    printf("%d", res);

    return 0;
}
