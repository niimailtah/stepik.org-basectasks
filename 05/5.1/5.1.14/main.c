#include <stdio.h>
#include <math.h>

int main(void)
{
    double x;
    double e_x, math_e_x;

    scanf("%lf", &x);

    math_e_x = exp(x);
    e_x = (double)1 +
        ((double)x / (1)) +
        ((double)x * x / (1 * 2)) +
        ((double)x * x * x / (1 * 2 * 3)) +
        ((double)x * x *x * x / (1 * 2 * 3 * 4)) +
        ((double)x * x * x * x * x / (1 * 2 * 3 * 4 * 5));

    printf("%.6lf\n%.6lf", math_e_x, e_x);

    return 0;
}
