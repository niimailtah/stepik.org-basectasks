#include <stdio.h>

int main(void)
{
    int r, h;
    double v1, v2;
    double pi = 3.14159265358979323846;

    scanf("%d%d", &r, &h);

    v1 = pi * h * r * r;
    v2 = v1 / 3;

    printf("%.2lf %.2lf", v1, v2);

    return 0;
}
