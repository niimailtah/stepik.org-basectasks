#include <stdio.h>

int main(void)
{
    printf("1! =%8.d\n", 1);
    printf("2! =%8.d\n", 1 * 2);
    printf("3! =%8.d\n", 1 * 2 * 3);
    printf("4! =%8.d\n", 1 * 2 * 3 * 4);
    printf("5! =%8.d\n", 1 * 2 * 3 * 4 * 5);
    printf("6! =%8.d\n", 1 * 2 * 3 * 4 * 5 * 6);
    printf("7! =%8.d\n", 1 * 2 * 3 * 4 * 5 * 6 * 7);
    printf("8! =%8.d\n", 1 * 2 * 3 * 4 * 5 * 6 * 7 * 8);
    printf("9! =%8.d\n", 1 * 2 * 3 * 4 * 5 * 6 * 7 * 8 * 9);
    printf("10!=%8.d\n", 1 * 2 * 3 * 4 * 5 * 6 * 7 * 8 * 9 * 10);

    return 0;
}
