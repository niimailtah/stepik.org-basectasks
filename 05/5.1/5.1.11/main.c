#include <stdio.h>

int main(void)
{
    int K, inverse;

    scanf("%d", &K);
    inverse = 1 - K;

    printf("%d", inverse);

    return 0;
}
