Цифры частей числа
================

Даны натуральные числа *M* и *N*. Вывести младшую цифру целой части и старшую цифру дробной части числа $\frac{M}{N}$


---
## Входные данные ##
Два натуральных числа *M* и *N*, записанных через пробел.


---
## Выходные данные ##
Два целых числа. Первое младшая цифра целой части числа $\frac{M}{N}$, второе - старшая цифра дробной части числа $\frac{M}{N}$


---
## Sample Input 1: ##
```
1554 155
```

---
## Sample Output 1: ##
```
0 0
```

---
## Sample Input 2: ##
```
1234 473
```

---
## Sample Output 2: ##
```
2 6
```
