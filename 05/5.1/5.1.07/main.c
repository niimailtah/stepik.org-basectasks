#include <stdio.h>

int main(void)
{
    int M, N, first_digit, first_after;

    scanf("%d%d", &M, &N);

    first_digit = (int)((double)M / N) % 10;
    first_after = (int)(((double)M / N) * 10) % 10;

    printf("%d %d", first_digit, first_after);

    return 0;
}
