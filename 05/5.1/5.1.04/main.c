#include <stdio.h>

int main(void)
{
    int k, hours, minuts;

    scanf("%d", &k);

    hours = k / 3600;
    minuts = (k / 60) % 60;

    printf("%d %d", hours, minuts);

    return 0;
}