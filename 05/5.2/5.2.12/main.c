#include <stdio.h>

int main(void)
{
    int k, res;
    int days_in_week = 7;

    scanf("%d", &k);

    res = ((k - 1) / days_in_week) + 1;
    printf("%d", res);

    return 0;
}
