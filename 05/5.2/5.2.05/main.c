#include <stdio.h>

int main(void)
{
    int x1, x2, x3, x4, x5;
    int sum = 0;

    scanf("%d%d%d%d%d", &x1, &x2, &x3, &x4, &x5);

    sum += x1 * x1;
    sum += x2 * x2;
    sum += x3 * x3;
    sum += x4 * x4;
    sum += x5 * x5;
    printf("%d", sum);

    return 0;
}
