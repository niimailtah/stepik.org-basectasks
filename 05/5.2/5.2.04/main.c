#include <stdio.h>
#include <math.h>

int main(void)
{
    int x1, x2, x3, x4, x5, parity;

    scanf("%d%d%d%d%d", &x1, &x2, &x3, &x4, &x5);

    parity = -pow(-1, x1);
    parity += -pow(-1, x2);
    parity += -pow(-1, x3);
    parity += -pow(-1, x4);
    parity += -pow(-1, x5);
    printf("%d", parity);

    return 0;
}
