#include <stdio.h>

int main(void)
{
    int k, res, first_digit, second_digit, third_digit;

    scanf("%d", &k);
    first_digit = k % 10;
    k /= 10;
    second_digit = k % 10;
    k /= 10;
    third_digit = k % 10;

    res = first_digit * 10 + second_digit * 100 + third_digit;
    printf("%d", res);

    return 0;
}
