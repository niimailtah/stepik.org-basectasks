#include <stdio.h>
#include <math.h>

int main(void)
{
    int k, parity;

    scanf("%d", &k);

    parity = pow(-1, k);
    printf("%d", parity);

    return 0;
}
