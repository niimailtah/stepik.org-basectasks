#include <stdio.h>

int main(void)
{
    int k, res;
    int sec_in_hour = 3600;

    scanf("%d", &k);

    res = k % sec_in_hour;
    printf("%d", res);

    return 0;
}
