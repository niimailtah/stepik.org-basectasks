#include <stdio.h>

int main(void)
{
    int k, res;
    int sec_in_minute = 60;

    scanf("%d", &k);

    res = k % sec_in_minute;
    printf("%d", res);

    return 0;
}
