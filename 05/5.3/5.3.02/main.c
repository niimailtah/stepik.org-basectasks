#include <stdio.h>
#include <math.h>

int main(void)
{
    int k, res;

    scanf("%d", &k);

    res = pow(2, k / 3);
    printf("%d", res);

    return 0;
}
