#include <stdio.h>

int main(void)
{
    int F, D;
    double height;
    double inch = 25.4;
    double foot = 12 * inch;
    double mm_in_m = 1000;

    scanf("%d%d", &F, &D);

    height = (F * foot + D * inch) / mm_in_m;
    printf("%d'%d\" = %.2lfm.", F, D, height);

    return 0;
}
