#include <stdio.h>
#include <math.h>

int main(void)
{
    int k;
    double N;
    double N0 = 75.0;
    double T = 5570.0;

    scanf("%d", &k);
    N =  N0 * exp(-(log(2)/T)*k);

    printf("%.2lf", N);

   return 0;
}
