#include <stdio.h>

int main(void)
{
    int X, x1, x2, x3, x4, Y, y1, y2, y3, y4;
    const int k = -7;

    scanf("%d", &Y);
    y1 = Y % 10;
    Y /= 10;
    y2 = Y % 10;
    Y /= 10;
    y3 = Y % 10;
    Y /= 10;
    y4 = Y % 10;
    x1 = (k + y1 + 10)%10;
    x2 = (k + y2 + 10)%10;
    x3 = (k + y3 + 10)%10;
    x4 = (k + y4 + 10)%10;
    X = x2 * 1000 + x1 * 100 + x4 * 10 + x3;

    printf("%d", X);

   return 0;
}
