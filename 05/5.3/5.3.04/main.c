#include <stdio.h>

int main(void)
{
    int M;
    double weight;
    double pound = 454;
    double kg_in_g = 1000;

    scanf("%d", &M);

    weight = M * pound / kg_in_g;
    printf("%.2lf", weight);

    return 0;
}
