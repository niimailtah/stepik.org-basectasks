#include <stdio.h>

int main(void)
{
    int k, x1, x2, x3, x4, y1, y2, y3, y4;

    scanf("%d%d%d%d%d", &k, &x1, &x2, &x3, &x4);
    y1 = (k + x1)%26;
    y2 = (k + x2)%26;
    y3 = (k + x3)%26;
    y4 = (k + x4)%26;

    printf("%d %d %d %d", y1, y2, y3, y4);

    return 0;
}
