#include <stdio.h>

int main(void)
{
    int n, k, D;

    scanf("%d%d", &k, &n);
    D = n % 10;
    n /= 10;
    D += n % 10 * k;
    n /= 10;
    D += n % 10 * k * k;
    n /= 10;
    D += n % 10 * k * k * k;

    printf("%d", D);

    return 0;
}
