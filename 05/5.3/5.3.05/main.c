#include <stdio.h>

int main(void)
{
    int F;
    double C;

    scanf("%d", &F);

    C = (F - 32) / 1.8;
    printf("%.2lf", C);

    return 0;
}
