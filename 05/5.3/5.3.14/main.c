#include <stdio.h>
#include <math.h>

int main(void)
{
    int h;
    double density;
    double density0 = 1.29;
    double z = 1.25e-4;

    scanf("%d", &h);
    density = density0 * exp(-h * z);

    printf("%.2lf", density);

   return 0;
}
