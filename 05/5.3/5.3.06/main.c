#include <stdio.h>

int main(void)
{
    int B, D;

    scanf("%d", &B);
    D = B % 10;
    B /= 10;
    D += B % 10 * 2;
    B /= 10;
    D += B % 10 * 4;
    B /= 10;
    D += B % 10 * 8;

    printf("%d", D);

    return 0;
}
