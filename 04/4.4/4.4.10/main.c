#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(void)
{
    int rand_digit, s, e;

    scanf("%d%d", &s, &e);
    srand(time(NULL));
    rand_digit = rand()%(e - s) + s;
    printf("%d", rand_digit);

    return 0;
}
