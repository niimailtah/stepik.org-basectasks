#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(void)
{
    int rand_digit, n;

    scanf("%d", &n);
    srand(time(NULL));
    rand_digit = rand()%n;
    printf("%d", rand_digit);

    return 0;
}
