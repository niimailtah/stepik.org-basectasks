#include <stdio.h>
 
int main()
{
    int n, s;
 
    scanf("%d",&n);
    
    s = 0;
    while (n > 0)
    {
        s += n%10;
        n /= 10;
    }
 
    printf("%d", s);
    return 0;
}
