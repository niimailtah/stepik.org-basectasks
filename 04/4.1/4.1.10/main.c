#include <stdio.h>
 
int main()
{
    unsigned int num, oddness;
 
    scanf("%d", &num);
    oddness = !(num % 2);
 
    printf("%d", oddness);
    return 0;
}
