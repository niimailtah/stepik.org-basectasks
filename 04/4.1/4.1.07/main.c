#include <stdio.h>
 
int main()
{
    int begin, end;
    double tax, sum;
 
    scanf("%d%d%lf",&begin, &end, &tax);
    sum = (end - begin) * tax;
 
    printf("%.2f", sum);
    return 0;
}
