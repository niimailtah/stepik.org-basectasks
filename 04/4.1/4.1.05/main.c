#include <stdio.h>

int main()
{
    int a, b, perimeter;

    scanf("%d%d", &a, &b);
    perimeter = a + a + b + b;

    printf("%d", perimeter);

    return 0;
}
