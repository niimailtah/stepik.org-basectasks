#include <stdio.h>
 
int main()
{
    unsigned int num, parity;
 
    scanf("%d", &num);
    parity = num % 2;
 
    printf("%d", parity);
    return 0;
}
