#include <stdio.h>
 
int main()
{
    int angle;
    double rad;
    double pi = 3.1415926;
 
    scanf("%d",&angle);
    rad = angle * pi / 180;
 
    printf("%.2f", rad);
    return 0;
}
