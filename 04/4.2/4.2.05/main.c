#include <stdio.h>
#include <math.h>
 
int main()
{
    double x1, x2;
    int dest;

    scanf("%lf%lf", &x1, &x2);
    dest = fabs(x2 - x1);
 
    printf("%d", dest);
    return 0;
}
