#include <stdio.h>
#include <math.h>
 
int main()
{
    int n, N;
    
    scanf("%d", &N);
    n = pow(2, N);
 
    printf("%d", n);
    return 0;
}
