#include <stdio.h>
#include <math.h>
 
int main()
{
    double a, b, gamma, s;
    double pi = 3.141593;

    scanf("%lf%lf%lf", &a, &b, &gamma);
    s =  .5*a*b*sin(gamma*pi/180);
 
    printf("%.2lf", s);
    return 0;
}
