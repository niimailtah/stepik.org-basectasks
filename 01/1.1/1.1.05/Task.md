Hello, World!
============

Напишите программу, которая выводит на экран следующие строки:

```
Hello, World!
The world is yours!
```

---
## Sample Input: ##

---
## Sample Output: ##
```
Hello, World!
The world is yours!
```
