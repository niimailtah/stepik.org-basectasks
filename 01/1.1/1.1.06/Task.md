Кто-же Neo?
==========

Напишите программу, которая выводит на экран следующие строки:

```
NICKNAME: Neo
CITY: St-Petersburg
AGE: 35
HEIGHT: 180
WEIGHT: 75
```

---
## Sample Input: ##

---
## Sample Output: ##
```
NICKNAME: Neo
CITY: St-Petersburg
AGE: 35
HEIGHT: 180
WEIGHT: 75
```
