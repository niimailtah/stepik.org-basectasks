#include <stdio.h> 

int main(void)
{
    double x, y;
    int quadrant;

    scanf("%lf%lf", &x, &y);
    quadrant = (x > 0) * (y > 0) +
               (x < 0) * (y > 0) * 2 +
               (x < 0) * (y < 0) * 3 +
               (x > 0) * (y < 0) * 4;

    printf("%d", quadrant);

    return 0;
}
