#include <stdio.h>
#include <math.h> 

int main(void)
{
    int x1, y1, x2, y2;
    double r1, r2;

    scanf("%d%d%d%d", &x1, &y1, &x2, &y2);

    r1 = sqrt(x1 * x1 + y1 * y1);
    r2 = sqrt(x2 * x2 + y2 * y2);
    if (r1 > r2)
    {
        printf("%d", 2);
    }
    else
    {
        printf("%d", 1);
    }

    return 0;
}
