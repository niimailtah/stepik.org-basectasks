#include <stdio.h>

int main(void)
{
    int min, summ;

    scanf("%d", &min);

    summ = 350;
    if (min > 500)
    {
        summ += (min - 500)*2;
    }
    printf("%d", summ);

    return 0;
}
