#include <stdio.h>
#include <stdbool.h>

int main(void)
{
    int floor_a, floor_b, floor_c;
    bool a_more_b, a_more_c, b_more_c;
    int first_stop, second_stop, third_stop;

    scanf("%d%d%d", &floor_a, &floor_b, &floor_c);

    a_more_b = floor_a > floor_b;
    a_more_c = floor_a > floor_c;
    b_more_c = floor_b > floor_c;
    if (a_more_b)
    {
        if (a_more_c)
        {
            if (b_more_c)
            {
                first_stop = floor_c, second_stop = floor_b, third_stop = floor_a;
            }
            else
            {
                first_stop = floor_b, second_stop = floor_c, third_stop = floor_a;
            }
        }
        else
        {
            first_stop = floor_b, second_stop = floor_a, third_stop = floor_c;
        }
    }
    else
    {
        if (b_more_c)
        {
            if (a_more_c)
            {
                first_stop = floor_c, second_stop = floor_a, third_stop = floor_b;
            }
            else
            {
                first_stop = floor_a, second_stop = floor_c, third_stop = floor_b;
            }
        }
        else
        {
            first_stop = floor_a, second_stop = floor_b, third_stop = floor_c;
        }
    }
    printf("%d %d %d", first_stop, second_stop, third_stop);

    return 0;
}
