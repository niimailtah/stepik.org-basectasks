#include <stdio.h>
#include <stdbool.h>

int main(void)
{
    int age;

    scanf("%d", &age);

    if (age <= 6)
    {
        printf("дошкольник");
    }
    else
    {
        if (age <= 18)
        {
            printf("школьник");
        }
        else
        {
            if (age <= 59)
            {
                printf("рабочий");
            }
            else
            {
                printf("пенсионер");
            }
        }
    }

    return 0;
}
