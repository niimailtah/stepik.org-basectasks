#include <stdio.h>
#include <stdbool.h>

int main(void)
{
    int day, month;
    bool correct;

    scanf("%d%d", &day, &month);

    correct = false;
    if (month >= 1 && month <= 12)
    {
        switch (month)
        {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                if (day >= 1 && day <= 31)
                {
                    correct = true;
                }
                break;
            case 2:
                if (day >= 1 && day <= 29)
                {
                    correct = true;
                }
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                if (day >= 1 && day <= 30)
                {
                    correct = true;
                }
                break;
            default:
                break;           
        }
    }
    if (correct)
    {
        printf("correct");
    }
    else
    {
        printf("error");
    }
    

    return 0;
}
