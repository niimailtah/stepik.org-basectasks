#include <stdio.h> 

int main(void)
{
    int a, b, c, positive;

    scanf("%d%d%d", &a, &b, &c);

    positive = 0;
    if (a > 0)
    {
        positive++;
    }
    if (b > 0)
    {
        positive++;
    }
    if (c > 0)
    {
        positive++;
    }
    printf("%d\n", positive);

    return 0;
}
