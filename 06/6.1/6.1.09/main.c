#include <stdio.h> 

int main(void)
{
    int m, d, n;

    scanf("%d %d", &m, &d);

    n = 0;
    switch (m)
    {
        case 12:
            n += 30;
        case 11:
            n += 31;
        case 10:
            n += 30;
        case 9 :
            n += 31;
        case 8 :
            n += 31;
        case 7 :
            n += 30;
        case 6 :
            n += 31;
        case 5 :
            n += 30;
        case 4 :
            n += 31;
        case 3 :
            n += 28;
        case 2 :
            n += 31;
        case 1 :
            n += d;
        default:
            break;
    }
    printf("%d", n);

    return 0;
}
