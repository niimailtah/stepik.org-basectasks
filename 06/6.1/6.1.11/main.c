#include <stdio.h> 

int main(void)
{
    int K, decade, last_digit;

    scanf("%d", &K);
    decade = K / 10;
    last_digit = K % 10;

    switch (decade)
    {
        case 1 :
            printf("Мне %d лет", K);
            break;
        default:
            switch (last_digit)
            {
                case 0 :
                case 5 :
                case 6 :
                case 7 :
                case 8 :
                case 9 :
                    printf("Мне %d лет", K);
                    break;
                case 1 :
                    printf("Мне %d год", K);
                    break;
                case 2 :
                case 3 :
                case 4 :
                    printf("Мне %d года", K);
                    break;
                default:
                    break;
            }
            break;
    }

    return 0;
}
