#include <stdio.h>

int main(void)
{
    int k, parity;

    scanf("%d", &k);
    parity = k % 2;

    switch (parity)
    {
        case 0 :
            printf("Не любит\n");
            break;
        case 1 :
            printf("Любит\n");
            break;
        default:
            break;
    }

    return 0;
}
