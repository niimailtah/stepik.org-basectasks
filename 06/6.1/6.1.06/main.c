#include <stdio.h>

int main(void)
{
    char c;
    int a, b;

    scanf("%c%d%d", &c, &a, &b);

    switch (c)
    {
        case '+' :
            printf("%.2lf\n", (double)a + b);
            break;
        case '-' :
            printf("%.2lf\n", (double)a - b);
            break;
        case '*' :
            printf("%.2lf\n", (double)a * b);
            break;
        case '/' :
            printf("%.2lf\n", (double)a / b);
            break;
        default:
            printf("ERROR!\n");
            break;
    }

    return 0;
}
