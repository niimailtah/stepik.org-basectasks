#include <stdio.h> 

int main(void)
{
    int k, X, p;

    scanf("%d %d", &k, &X);

    p = 1;
    switch (k)
    {
        case 4 :
            p *= X%10;
            X /= 10;
        case 3 :
            p *= X%10;
            X /= 10;
        case 2 :
            p *= X%10;
            X /= 10;
        case 1 :
            p *= X%10;
        default:
            break;
    }
    printf("%d", p);

    return 0;
}
