#include <stdio.h>

int main(void)
{
    int i, j, N;
    int a[100][100] = {0};

    scanf("%d", &N);
    for (i = 0; i < N; ++i)
    {
        for (j = 0; j < N; ++j)
        {
            if (i % 2 == 1)
            {
                a[i][j] = N - j;
            }
            else
            {
                a[i][j] = j + 1;
            }
            
        }
    }

    for (i = 0; i < N; ++i)
    {
        for (j = 0; j < N; ++j)
        {
            printf("%d ", a[i][j]);
        }
        printf("\n");
    }

    return 0;
}
