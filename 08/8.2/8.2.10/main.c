#include <stdio.h>

int main(void)
{
    int i, j, N, K, shift, cur;
    int a[20][20] = {0};

    scanf("%d", &N);

    // input
    for (i = 0; i < N; ++i)
    {
        for (j = 0; j < N; ++j)
        {
            scanf("%d", &a[i][j]);
            a[i][N + j] = a[i][j];
        }
    }
    scanf("%d", &K);
    shift = K % N;

    // output
    for (i = 0; i < N; ++i)
    {
        for (j = N - shift; j < N * 2 - shift; ++j)
        {
            printf("%d ", a[i][j]);
        }
        printf("\n");
    }

    return 0;
}
