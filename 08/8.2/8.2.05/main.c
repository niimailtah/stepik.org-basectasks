#include <stdio.h>

int main(void)
{
    int i, j, value, N;
    int a[100][100] = {0};

    scanf("%d", &N);
    for (i = 0; i < N; ++i)
    {
        value = i + 1;
        for (j = 0; j < N && value > 0; ++j, --value)
        {
            a[i][j] = value;
        }
        for (value = 2; j < N; ++j, ++value)
        {
            a[i][j] = value;
        }
    }

    for (i = 0; i < N; ++i)
    {
        for (j = 0; j < N; ++j)
        {
            printf("%d ", a[i][j]);
        }
        printf("\n");
    }

    return 0;
}
