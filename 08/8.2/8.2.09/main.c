#include <stdio.h>

int main(void)
{
    int i, j, N, M, index_max, index_min, max, min, cur;
    int a[10][10] = {0};
    int sum[10] = {0};

    scanf("%d%d", &N, &M);

    // input
    for (i = 0; i < N; ++i)
    {
        for (j = 0; j < M; ++j)
        {
            scanf("%d", &a[i][j]);
        }
    }

    // calc
    for (j = 0; j < M; ++j)
    {
        for (i = 0; i < N; ++i)
        {
            sum[j] += a[i][j];
        }
    }

    // min/max
    index_max = 0;
    index_min = 0;
    max = sum[0];
    min = sum[0];
    for (j = 1; j < M; ++j)
    {
        if (sum[j] > max)
        {
            index_max = j;
            max = sum[j];
        }
        if (sum[j] < min)
        {
            index_min = j;
            min = sum[j];
        }
    }

    // swap
    for (i = 0; i < N; ++i)
    {
        cur = a[i][index_min];
        a[i][index_min] = a[i][index_max];
        a[i][index_max] = cur;
    }

    // output
    for (i = 0; i < N; ++i)
    {
        for (j = 0; j < M; ++j)
        {
            printf("%d ", a[i][j]);
        }
        printf("\n");
    }
    for (j = 0; j < M; ++j)

    return 0;
}
