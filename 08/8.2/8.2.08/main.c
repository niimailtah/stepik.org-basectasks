#include <stdio.h>

int main(void)
{
    int i, j, N, M;
    int a[10][10] = {0};
    int sum[10] = {0};

    scanf("%d%d", &N, &M);

    for (i = 0; i < N; ++i)
    {
        for (j = 0; j < M; ++j)
        {
            scanf("%d", &a[i][j]);
        }
    }
    for (j = 0; j < M; ++j)
    {
        for (i = 0; i < N; ++i)
        {
            sum[j] += a[i][j];
        }
    }
    for (j = M - 1; j >= 0; --j)
    {
        printf("%d ", sum[j]);
    }
    return 0;
}
