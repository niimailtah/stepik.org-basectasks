#include <stdio.h>

int main(void)
{
    int i, j, N, s1, s2;
    int a[10][10] = {0};

    scanf("%d", &N);
    for (i = 0; i < N; ++i)
    {
        for (j = 0; j < N; ++j)
        {
            scanf("%d", &a[i][j]);
        }
    }
    s1 = 0;
    s2 = 0;
    for (i = 0; i < N; ++i)
    {
        for (j = i + 1; j < N; ++j)
        {
            s1 += a[i][j];
        }
    }
    for (i = N - 1; i > 0; --i)
    {
        for (j = N - 1; j >= N - i; --j)
        {
            s2 += a[i][j];
        }
    }

    if (s1 > s2)
    {
        printf("%d %d", s2, s1);
    }
    else
    {
        printf("%d %d", s1, s2);
    }
    

    return 0;
}
