#include <stdio.h>

int main(void)
{
    int i, N, d1, d2;
    int a[10][10] = {0};

    scanf("%d", &N);
    for (i = 0; i < N; ++i)
    {
        for (int j = 0; j < N; ++j)
        {
            scanf("%d", &a[i][j]);
        }
    }
    d1 = 1;
    d2 = 1;
    for (i = 0; i < N; ++i)
    {
        d1 *= a[i][i];
        d2 *= a[i][N - i - 1];
    }

    if (d1 > d2)
    {
        printf("%d %d", d1, d2);
    }
    else
    {
        printf("%d %d", d2, d1);
    }
    

    return 0;
}
