#include <stdio.h>
#include <stdbool.h>

int main(void)
{
    int i, j, N, M, top, bottom, left, right, index;
    int a[10][10] = {0};
    int source[100] = {0};

    scanf("%d%d", &N, &M);

    index = 0;
    for (i = 0; i < N; ++i)
    {
        for (j = 0; j < M; ++j)
        {
            source[index] = index + 1;
            index++;
        }
    }

    top = 0;
    bottom = N - 1;
    left = 0;
    right = M - 1;
    index = 0;
    while (true)
    {
        if (left > right)
        {
            break;
        }
 
        for (i = left; i <= right; i++)
        {
            a[top][i] = source[index++];
        }
        top++;
 
        if (top > bottom)
        {
            break;
        }
 
        for (i = top; i <= bottom; i++)
        {
            a[i][right] = source[index++];
        }
        right--;
 
        if (left > right)
        {
            break;
        }
 
        for (i = right; i >= left; i--)
        {
            a[bottom][i] = source[index++];
        }
        bottom--;
 
        if (top > bottom)
        {
            break;
        }
 
        for (i = bottom; i >= top; i--)
        {
            a[i][left] = source[index++];
        }
        left++;
    }

    // output
    for (i = 0; i < N; ++i)
    {
        for (j = 0; j < M; ++j)
        {
            printf("%3d", a[i][j]);
        }
        printf("\n");
    }

    return 0;
}
