#include <stdio.h>

int main(void)
{
    int i, j, N, M;
    int a[10][10] = {0};

    scanf("%d%d", &N, & M);
    for (i = 0; i < N; ++i)
    {
        for (j = 0; j < M; ++j)
        {
            scanf("%d", &a[i][j]);
        }
    }

    for (j = 0; j < M; ++j)
    {
        for (i = 0; i < N; ++i)
        {
            printf("%d ", a[i][j]);
        }
        printf("\n");
    }

    return 0;
}
