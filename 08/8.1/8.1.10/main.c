#include <stdio.h>

int main(void)
{
    int i, N, min, max;
    int a[100] = {0};

    scanf("%d", &N);
    for (i = 0; i < N; ++i)
    {
        scanf("%d", &a[i]);
    }

    min = a[0];
    max = a[0];
    for (i = 1; i < N; ++i)
    {
        if (a[i] > max)
        {
            max = a[i];
        }
        if (a[i] < min)
        {
            min = a[i];
        }
    }
    for (i = 0; i < N; ++i)
    {
        if (a[i] == max)
        {
            a[i] = min;
        }
        else
        {
            if (a[i] == min)
            {
                a[i] = max;
            }
        }
    }

    for (i = 0; i < N; ++i)
    {
        printf("%d ", a[i]);
    }

    return 0;
}
