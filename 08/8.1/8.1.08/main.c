#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

int main(void)
{
    int i, j, k, N, summ;
    int a[100] = {0};
    int b[100] = {0};
    double average;

    scanf("%d", &N);
    for (i = 0; i < N; ++i)
    {
        scanf("%d", &a[i]);
    }

    summ = 0;
    for (i = 0; i < N; ++i)
    {
        summ += a[i];
    }
    average = (double)summ / N;

    j = 0;
    for (i = 0; i < N; ++i)
    {
        if (a[i] > average)
        {
            b[j] = a[i];
            j++;
        }
    }

    for (i = 0; i < N; ++i)
    {
        if (a[i] <= average)
        {
            b[j] = a[i];
            j++;
        }
    }

    for (k = 0; k < N; ++k)
    {
        printf("%d ", b[k]);
    }

    return 0;
}
