#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

int main(void)
{
    int i, N;
    int a[100] = {0};

    scanf("%d", &N);

    for (i = 0; i < N; ++i)
    {
        scanf("%d", &a[i]);
    }
    for (i = N - 1; i >= 0; --i)
    {
        printf("%d ", a[i]);
    }

    return 0;
}
