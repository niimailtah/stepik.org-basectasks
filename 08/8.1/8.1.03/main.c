#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

int main(void)
{
    int i, N;
    int a[100] = {0};
    bool palindrome = true;

    scanf("%d", &N);

    for (i = 0; i < N; ++i)
    {
        scanf("%d", &a[i]);
    }
    for (i = 0; i < N / 2; ++i)
    {
        if (a[i] != a[N - i - 1])
        {
            palindrome = false;
            break;
        }
    }
    if (palindrome)
    {
        printf("YES");
    }
    else
    {
        printf("NO");
    }

    return 0;
}
