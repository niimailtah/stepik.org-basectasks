#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

int main(void)
{
    int i, N, summ;
    int a[100] = {0};
    double average;

    scanf("%d", &N);
    for (i = 0; i < N; ++i)
    {
        scanf("%d", &a[i]);
    }

    summ = 0;
    for (i = 0; i < N; ++i)
    {
        summ += a[i];
    }

    average = (double)summ / N;    
    printf("%.2lf", average);

    return 0;
}
