#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

int main(void)
{
    int i, N;
    int a[100] = {0};
    bool is_any_print = false;

    scanf("%d", &N);
    for (i = 0; i < N; ++i)
    {
        scanf("%d", &a[i]);
    }

    for (i = 1; i < N - 1; ++i)
    {
        if (a[i] < a[N - 1] && a[i] > a[0])
        {
            printf("%d ", a[i]);
            is_any_print = true;
        }
    }
    if (!is_any_print)
    {
        printf("%d", 0);
    }

    return 0;
}
