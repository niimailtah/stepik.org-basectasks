#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

int main(void)
{
    int i, N, cur;
    int a[100] = {0};

    scanf("%d", &N);
    for (i = 0; i < N; ++i)
    {
        scanf("%d", &a[i]);
    }

    for (i = 0; i < N / 2; ++i)
    {
        cur = a[i];
        a[i] = a[N / 2 + i];
        a[N / 2 + i] = cur;
    }

    for (i = 0; i < N; ++i)
    {
        printf("%d ", a[i]);
    }

    return 0;
}
