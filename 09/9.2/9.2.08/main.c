#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>

int main(void)
{
    int steps, len, x, y;
    char dir[7] = {0};

    scanf("%d\n", &steps);

    x = 0;
    y = 0;
    for (int step = 0; step < steps; ++step)
    {
        scanf("%s %d\n", dir, &len);
        if (strcmp(dir, "North") == 0)
        {
            y += len;
        }
        else
        {
            if (strcmp(dir, "South") == 0)
            {
                y -= len;
            }
            else
            {
                if (strcmp(dir, "East") == 0)
                {
                    x += len;
                }
                else
                {
                    if (strcmp(dir, "West") == 0)
                    {
                        x -= len;
                    }
                    else
                    {
                        printf("Ooops.\n");
                        break;
                    }
                }
            }
        }
    }

    printf("%d %d", x, y);

    return 0;
}
