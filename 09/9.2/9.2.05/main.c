#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>

int main(void)
{
    int number, power_2;
    char str[100] = {0};

    fgets(str, 100, stdin);
    number = str[strlen(str) - 2] - 48;
    power_2 = 2;
    for (int i = strlen(str) - 3; i >= 0; --i)
    {
        number += (str[i] - 48) * power_2;
        power_2 *= 2;
    }

    printf("%d", number);

    return 0;
}
