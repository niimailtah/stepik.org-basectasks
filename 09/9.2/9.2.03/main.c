#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>

int main(void)
{
    char str[100] = {0};
    int count = 0;
    bool in_word = false;

    fgets(str, 100, stdin);
    for (int i = 0; str[i] != '\n' && str[i] != '\0'; ++i)
    {
        if (in_word)
        {
            if (str[i] == ' ')
            {
                in_word = false;
                count++;
            }
        }
        else
        {
            if (str[i] != ' ')
            {
                in_word = true;
            }
        }
    }
    if (in_word)
    {
        count++;
    }

    printf("%d", count);

    return 0;
}
