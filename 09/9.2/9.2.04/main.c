#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>

int main(void)
{
    char ch;
    char str[100] = {0};
    int idx = -1;

    scanf("%c\n", &ch);
    fgets(str, 100, stdin);
    for (int i = 0; i < strlen(str); ++i)
    {
        if (str[i] == ch)
        {
            idx = i;
            break;
        }
    }

    printf("%d", idx);

    return 0;
}
