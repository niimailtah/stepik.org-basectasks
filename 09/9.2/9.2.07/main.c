#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>

int main(void)
{
    char source[100] = {0};
    char dest[100] = {0};
    char prev_ch = '\0';
    int idx = 0;

    fgets(source, 100, stdin);
    for (int i = 0; source[i] != '\n' && source[i] != '\0'; ++i)
    {
        if (source[i] != ' ')
        {
            dest[idx++] = source[i];
        }
        else
        {
            if (prev_ch != ' ')
            {
                dest[idx++] = source[i];
            }
        }
        prev_ch = source[i];
    }

    printf("%s", dest);

    return 0;
}
