#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>

int main(void)
{
    char ch;

    scanf("%c", &ch);

    if (isdigit(ch))
    {
        printf("digit");
    }
    else
    {
        if (isalpha(ch))
        {
            printf("en");
        }
        else
        {
            printf("error");
        }
    }



    return 0;
}
