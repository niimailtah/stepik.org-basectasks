#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>

int main(void)
{
    int K;
    int last = 'Z';

    scanf("%d", &K);
    // K = 3;

    for (int i = last - K + 1; i < last + 1; ++i)
    {
        printf("%c", i);
    }

    return 0;
}
