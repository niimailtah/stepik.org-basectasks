#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>

int main(void)
{
    int K;

    scanf("%d", &K);
    printf("%c%c", K + 64, K + 96);

    return 0;
}
