#include <stdio.h>
#include <stdbool.h>

int main(void)
{
    char ch;

    scanf("%c", &ch);

    // output
    printf("%c %c", ch - 1, ch + 1);

    return 0;
}
