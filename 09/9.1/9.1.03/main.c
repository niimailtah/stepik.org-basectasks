#include <stdio.h>
#include <stdbool.h>

int main(void)
{
    char ch;

    scanf("%c", &ch);
    ch -= 32;

    // output
    printf("%c", ch);

    return 0;
}
