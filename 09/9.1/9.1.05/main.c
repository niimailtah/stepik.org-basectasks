#include <stdio.h>
#include <string.h>
#include <stdbool.h>

int main(void)
{
    int N, i;
    char ch;
    char count[26] = {0};
    // char s[] = "AdsaadLesi";

    scanf("%d", &N);
    // N = 10;

    for (i = 0; i <= N; ++i)
    {
        scanf("%c ", &ch);
        // ch = s[i];
        if (ch > 90)
        {
            ch -= 32;
        }
        count[ch - 65]++;
    }

    // output
    for (i = 0; i < sizeof(count); ++i)
    {
        printf("%d ", count[i]);
    }

    return 0;
}
