#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>

int main()
{
    int j;
    bool is_complete = true;
    char from[51] = {0};
    char to[51] = {0};

    fgets(from, 50, stdin);
    fgets(to, 50, stdin);

    for (int i = 0; i < strlen(to); ++i)
    {
        for (j = 0; j < strlen(from); ++j)
        {
            if (to[i] == from[j])
            {
                from[j] = toupper(from[j]);
                break;
            }
        }
        if (j == strlen(from))
        {
            is_complete = false;
            break;
        }
    }

    if (is_complete)
    {
        printf("yes");
    }
    else
    {
        printf("no");
    }

    return 0;
}
