#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>

int main(void)
{
    char town1[55] = {0};
    char town2[55] = {0};
    bool is_chain = false;

    fgets(town1, 55, stdin);
    fgets(town2, 55, stdin);
    if (toupper(town1[strlen(town1) - 2]) == town2[0] ||
        tolower(town1[0]) == town2[strlen(town2) - 2])
    {
        is_chain = true;
    }

    if (is_chain)
    {
        printf("yes");
    }
    else
    {
        printf("no");
    }

    return 0;
}
