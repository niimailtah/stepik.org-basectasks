#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>

int main(void)
{
    char one_way_ticket[10] = {0};

    fgets(one_way_ticket, 10, stdin);
    if (one_way_ticket[0] - '0' +
        one_way_ticket[1] - '0' +
        one_way_ticket[2] - '0' ==
        one_way_ticket[3] - '0' +
        one_way_ticket[4] - '0' +
        one_way_ticket[5] - '0')
    {
        printf("yes");
    }
    else
    {
        printf("no");
    }

    return 0;
}
