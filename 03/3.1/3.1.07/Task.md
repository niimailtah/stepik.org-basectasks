Terminator
==========

Исправьте предложенную задачу так, чтобы она выводила следующий текст:

```
"I'll be back!" 
	(c)Terminator 
```
![](./I'll_Be_Back.jpg)

---
## Sample Input: ##

---
## Sample Output: ##

```
"I'll be back!"
	(c)Terminator
```
