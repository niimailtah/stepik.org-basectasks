#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

int main(void)
{
    int cur, prev;
    bool print_cur = false;

    scanf("%d", &cur);

    while (cur != -9999)
    {
        if (cur == 2517)
        {
            print_cur = true;
            scanf("%d", &cur);
            continue;
        }
        if (print_cur)
        {
            printf("%d ", cur);
        }
        scanf("%d", &cur);
    }

    return 0;
}
