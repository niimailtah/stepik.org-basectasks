#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

int main(void)
{
    int n;

    scanf("%d", &n);
    while (n != 0)
    {
        if (n > 0)
        {
            printf("%d ", n);
        }
        scanf("%d", &n);
    }

    return 0;
}
