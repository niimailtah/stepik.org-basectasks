#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

int main(void)
{
    int N, cur, prev;

    scanf("%d", &N);

    scanf("%d", &prev);
    printf("%d ", prev);
    for (int i = 1; i < N; ++i)
    {
        scanf("%d", &cur);
        if (cur != prev)
        {
            printf("%d ", cur);
            prev = cur;
        }
    }

    return 0;
}
