#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

int main(void)
{
    int cur, prev;
    bool ascending_sequence = true;

    scanf("%d%d", &prev, &cur);

    while (cur != -9999)
    {
        if (cur <= prev)
        {
            ascending_sequence = false;
            break;
        }
        prev = cur;
        scanf("%d", &cur);
    }

    if (ascending_sequence)
    {        
        printf("YES");
    }
    else
    {
        printf("NO");
    }
    return 0;
}
