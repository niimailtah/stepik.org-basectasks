#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

int main(void)
{
    int N;

    scanf("%d", &N);

    for (int i = 1; i <= N; ++i)
    {
        for (int j = 1; j <= i; ++j)
        {
            printf("%d ", j);
        }
        printf("\n");
    }

    return 0;
}
