#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

int main(void)
{
    int cur, prev, k;
    bool ascending_sequence = true;

    scanf("%d%d", &prev, &cur);

    k = 2;
    while (cur != -9999)
    {
        if (cur < prev)
        {
            ascending_sequence = false;
            break;
        }
        prev = cur;
        scanf("%d", &cur);
        k++;
    }

    if (ascending_sequence)
    {        
        k = 0;
    }
    printf("%d", k);

    return 0;
}
