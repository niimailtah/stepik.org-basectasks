#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

int main(void)
{
    int cur, summ;
    bool do_sum = false;

    scanf("%d", &cur);

    summ = 0;
    while (cur != -9999)
    {
        if (cur == 0)
        {
            do_sum = !do_sum;
            scanf("%d", &cur);
            continue;
        }
        if (do_sum)
        {
            summ += cur;
        }
        scanf("%d", &cur);
    }

    printf("%d", summ);

    return 0;
}
