#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

int main(void)
{
    int n;
    bool positive = false, sequence_is_present = false;

    scanf("%d", &n);
    if (n != -9999)
    {
        sequence_is_present = true;
        positive = true;
    }
    if (sequence_is_present)
    {
        while (n != -9999)
        {
            if (n < 0)
            {
                positive = false;
                break;
            }
            scanf("%d", &n);
        }
    }

    if (positive)
    {
        printf("YES");
    }
    else
    {
        printf("NO");
    }

    return 0;
}
