#include <stdio.h>

int main(void)
{
    int N, n, square;

    scanf("%d", &N);

    n = 1;
    square = n * n;
    while (square <= N)
    {
        printf("%d ", square);
        n++;
        square = n * n;
    }

    return 0;
}
