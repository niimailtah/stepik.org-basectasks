#include <stdio.h>

int main(void)
{
    unsigned int factorio, k;
    double e, epsilon, term;

    scanf("%lf", &epsilon);

    e = 2.5;
    k = 2;
    factorio = 2;
    term = 1. / factorio;
    do
    {
        factorio *= ++k;
        term = 1. / factorio;
        e += term;
    } while (term > epsilon);
    printf("%.8lf", e);

    return 0;
}
