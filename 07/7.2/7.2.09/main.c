#include <stdio.h>

int main(void)
{
    unsigned int N, k, n;

    scanf("%d", &N);

    n = 1;
    k = N / 10;
    while (k > 0)
    {
        N /= 10;
        k = N / 10;
        n++;
    }
    printf("%d", n);

    return 0;
}
