#include <stdio.h>
#include <stdbool.h>

int main(void)
{
    int cur, m, M;

    scanf("%d", &cur);
    m = cur;
    M = cur;
    while (true)
    {
        scanf("%d", &cur);
        if (cur == 0)
        {
            break;
        }
        if (cur > M)
        {
            M = cur;
        }
        if (cur < m)
        {
            m = cur;
        }
    }
    printf("%d %d", M, m);

    return 0;
}
