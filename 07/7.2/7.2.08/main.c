#include <stdio.h>
#include <stdbool.h>

int main(void)
{
    unsigned int N;
    bool power2;

    scanf("%d", &N);

    power2 = true;
    while (N != 1)
    {
        if (N % 2 == 0 && N != 0)
        {
            N /= 2;
        }
        else
        {
            power2 = false;
            break;
        }
    }
    if (power2)
    {
        printf("YES");
    }
    else
    {
        printf("NO");
    }

    return 0;
}
