#include <stdio.h>

int main(void)
{
    unsigned int N;

    scanf("%d", &N);

    while (N > 0)
    {
        printf("%d", N % 10);
        N /= 10;
    }

    return 0;
}
