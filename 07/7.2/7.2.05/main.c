#include <stdio.h>
#include <stdbool.h>

int main(void)
{
    int cur, summ;
    
    scanf("%d", &cur);
    summ = cur;
    while (cur)
    {
        scanf("%d", &cur);
        summ += cur;
    }
    printf("%d", summ);

    return 0;
}
