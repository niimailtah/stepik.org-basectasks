#include <stdio.h>
#include <stdbool.h>

int main(void)
{
    int cur;
    
    while (true)
    {
        scanf("%d", &cur);
        if (cur == -9999)
        {
            break;
        }
        printf("%d ", cur);
    }

    return 0;
}
