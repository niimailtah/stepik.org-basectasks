#include <stdio.h>
#include <stdbool.h>

int main(void)
{
    int cur;
    
    do
    {
        scanf("%d", &cur);
        printf("%d ", cur);
    } while (cur != -9999);

    return 0;
}
