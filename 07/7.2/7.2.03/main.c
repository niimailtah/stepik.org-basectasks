#include <stdio.h>

int main(void)
{
    int cur;
    
    scanf("%d", &cur);
    while (cur != -9999)
    {
        printf("%d ", cur);
        scanf("%d", &cur);
    }

    return 0;
}
