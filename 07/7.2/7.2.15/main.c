#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

int main(void)
{
    int A, B, a, b, nod, nok;

    scanf("%d%d", &A, &B);
    a = A;
    b = B;

    while (a != b)
    {
        if (a > b)
        {
            a -= b;
        }
        else
        {
            b -= a;
        }
    }
    nod = a;
    nok = abs(A * B)/nod;

    printf("%d", nok);

    return 0;
}
