#include <stdio.h>

int main(void)
{
    unsigned int N, r, two_power_r;

    scanf("%d", &N);

    r = 1;
    two_power_r = 2; // 2^1=2
    while (two_power_r < N)
    {
        r++;
        two_power_r = two_power_r*2;
    }
    printf("%d", r);

    return 0;
}
