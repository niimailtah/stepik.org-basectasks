#include <stdio.h>

int main(void)
{
    unsigned int N, k, two_power_k;

    scanf("%d", &N);

    k = 1;
    two_power_k = 2; // 2^1=2
    while (two_power_k <= N)
    {
        printf("%d ", k);
        k++;
        two_power_k = two_power_k*2;
    }

    return 0;
}
