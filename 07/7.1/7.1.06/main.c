#include <stdio.h>

int main(void)
{
    int k, factorio;

    scanf("%d", &k);

    factorio = 1;
    for (int i = 1; i <= k; ++i)
    {
        factorio *= i;
    }
    printf("%d ", factorio);

    return 0;
}
