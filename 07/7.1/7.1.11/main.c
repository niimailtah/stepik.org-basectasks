#include <stdio.h>
#include <stdbool.h>

int main(void)
{
    int N;
    bool simple;

    scanf("%d", &N);

    simple = true;
    for (int i = 2; i < N / 2; ++i)
    {
        if (N % i == 0)
        {
            simple = false;
            break;
        }
    }
    printf("%d ", simple);

    return 0;
}
