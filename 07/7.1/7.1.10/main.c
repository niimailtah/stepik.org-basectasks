#include <stdio.h>

int main(void)
{
    int N, num;

    scanf("%d", &N);

    num = 0;
    for (int i = 1; i <= N; ++i)
    {
        if (N % i == 0)
        {
            printf("%d ", i);
            num++;
        }
    }
    printf("\n%d ", num);

    return 0;
}
