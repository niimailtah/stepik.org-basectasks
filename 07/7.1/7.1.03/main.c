#include <stdio.h>

int main(void)
{
    int K, M;

    scanf("%d%d", &K, &M);

    if (K < 0)
    {
        K = 1;
    }
    for (int i = K; i <= M; ++i)
    {
        printf("%d ", i);
    }

    return 0;
}
