#include <stdio.h>

int main(void)
{
    int N, fib_N, fib_Nm1, fib_Nm2;

    scanf("%d", &N);

    fib_N = fib_Nm1 = fib_Nm2 = 1;
    for (int i = 3; i <= N; ++i)
    {
        fib_N = fib_Nm2 + fib_Nm1;
        fib_Nm2 = fib_Nm1;
        fib_Nm1 = fib_N;
    }
    printf("%d", fib_N);

    return 0;
}
