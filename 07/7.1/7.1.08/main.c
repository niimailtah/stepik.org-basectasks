#include <stdio.h>
#include <stdbool.h>

int main(void)
{
    int A, B;

    scanf("%d%d", &A, &B);

    for (int i = A; i <= B; ++i)
    {
        for (int j = 0; j <= i - A; ++j)
        {
            printf("%5d", i);
        }
    }

    return 0;
}
