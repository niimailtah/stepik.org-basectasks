#include <stdio.h>
#include <stdbool.h>

int main(void)
{
    int K, M, number;

    scanf("%d%d", &K, &M);

    if (K < 0)
    {
        K = 1;
    }
    number = 0;
    for (int i = K; i <= M; ++i)
    {
        number++;
        printf("%d ", i);
    }
    printf("\n%d ", number);

    return 0;
}
