int comp_gr(const int *i, const int *j)
{
    return *i - *j;
}

int comp_lt(const int *i, const int *j)
{
    return *j - *i;
}

void sort_arr(int arr[], int n, int fl)
{
    switch (fl)
    {
        case 0:
            qsort(arr, n, sizeof(int), (int(*) (const void *, const void *)) comp_gr);
            break;
        case 1:
            qsort(arr, n, sizeof(int), (int(*) (const void *, const void *)) comp_lt);
            break;
        default:
            break;
    }
    return;
}
