void swap(int * x, int * y)
{
    int cur;
    
    cur = *x;
    *x = *y;
    *y = cur;
}
