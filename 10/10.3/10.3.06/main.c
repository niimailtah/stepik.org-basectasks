void char_cesar(char * ch, int shift){
    *ch = ((*ch - 'a' + shift) % ('z' - 'a' + 1)) + 'a';
    return;
}
