void minmax(int * x, int * y){
    if (*x > *y)
    {
        int temp = *y;
        *y = *x;
        *x = temp;
    }
    return;
}
