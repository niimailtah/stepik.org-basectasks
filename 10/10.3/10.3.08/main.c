int gcd(int x, int y){
    while (x != y)
    {
        if (x > y)
        {
            x -= y;
        }
        else
        {
            y -= x;
        }
    }
    return x;
} 

void reduce_fraction(int * a, int * b){
    int GCD;

    GCD = gcd(*a, *b);
    *a = *a / GCD;
    *b = *b / GCD;

    return;
}
