void char_register(char * ch, int fl){
    if (isalpha(*ch))
    {
        switch (fl)
        {
            case 0:
                *ch = tolower(*ch);
                break;
            case 1:
                *ch = toupper(*ch);
                break;
            default:
                break;
        }
    }
    return;
}
