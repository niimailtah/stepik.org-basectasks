int linear_search(int arr[], int n, int arg){
    int found_idx = -1;

    for (int i = 0; i < n; ++i)
    {
        if (arr[i] == arg)
        {
            found_idx = i;
            break;
        }
    }
    return found_idx;
}
