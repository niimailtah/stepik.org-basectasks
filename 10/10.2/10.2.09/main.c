/*
Выше в программе объявлены указатели
    int * p_a
    int * p_b
    int * p_c
*/
    if (*p_a > *p_b)
    {
        *p_a = *p_b;
    }
    else
    {
        *p_b = *p_a;
    }
    if (*p_a > *p_c)
    {
        *p_a = *p_c;
        *p_b = *p_c;
    }
    else
    {
        *p_c = *p_a;
    }
    