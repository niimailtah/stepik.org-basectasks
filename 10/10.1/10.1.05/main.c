#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>

int factorial(int k)
{
    int fact = 1;

    for (int i = 1; i <= k; i++)
    {
        fact = fact * i;
    }    
    return fact;
}

int main()    
{    
    int number, fact;

    scanf("%d", &number);
    fact = factorial(number);
    printf("%d", fact);    

    return 0;  
}
