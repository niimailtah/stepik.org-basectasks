#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>

int square(int n, char ch)
{
    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            printf("%c", ch);
        }
        printf("\n");
    }
}

int main()
{    
    int number;
    char ch;

    scanf("%d %c", &number, &ch);
    square(number, ch);

    return 0;
}
